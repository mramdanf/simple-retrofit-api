# README #

## API URL and API Response ##

### Login ###


```
#!php
http://localhost/simple_retrofit/account/auth
```


Login successfull


```
#!json
{
  "status": true,
  "user_detail": [
    {
      "id": "1",
      "first_name": "ramdan",
      "last_name": "firdaus",
      "username": "ramdan",
      "password": "47bce5c74f589f4867dbd57e9ca9f808"
    }
  ]
}
```

Login failed


```
#!json

{
  "status": false,
  "user_detail": [0]
}
```

### Credential ###

Credential disini maksudnya adalah CRUD data Credential bukan autentikasi dan semua request pada Credential service harus melalui basic auth

Get List Credential


```
#!php
http://localhost/simple_retrofit/credentials/get
```


```
#!json
{
  "status": true,
  "credential_data": [
    {
      "id": "2",
      "description": "akun bitbucket",
      "username": "ramdan",
      "password": "aaa"
    }
  ]
}

```

Add dan Update data credential, dilakukan disatu URL (if credential_id exist means Update)


```
#!php
http://localhost/simple_retrofit/credentials/post

```


```
#!json

{
  "status": true,
  "post_msg": "Berhasil modify data"--"Gagal modify data"
}
```

Delete data Credential


```
#!php

http://localhost/simple_retrofit/credentials/delete
```


```
#!json
{
  "status": true,
  "post_msg": "Berhasil delete data"--"Gagal delete data"
}

```