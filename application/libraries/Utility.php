<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utility {

	private $CI;
	const HTTP_OK = 200;
	const HTTP_UNAUTHORIZED = 401;
	const HTTP_BAD_REQUEST = 400;
	const HTTP_INTERNAL_SERVER_ERROR = 500;
	const HTTP_CREATED = 201;
	const HTTP_NO_CONTENT = 204;
	const HTTP_NOT_FOUND = 404;

	const STATUS = 'status';
	const CRED_DATA = 'credential_data';

	const USER_DETAIL = 'user_detail';

	const POST_MSG = 'post_msg';

	public function __construct(){
		$this->CI =& get_instance();
	}

	public function checkUser($username, $password)
	{
		$this->CI->load->database();

		$this->CI->db->where('username', $username);
		$this->CI->db->where('password', md5($password));

		$query = $this->CI->db->get('user');

		return $query->result();
	}

	public function print_json($data, $http_code)
	{
		$this->CI->output
	      ->set_status_header($http_code)
	      ->set_content_type('application/json', 'utf-8')
	      ->set_output(json_encode($data, JSON_PRETTY_PRINT))
	      ->_display();
	    exit;
	}

	public function credential()
	{
		if (!isset($_SERVER['PHP_AUTH_USER'])) {
		    header('WWW-Authenticate: Basic realm="My Realm"');
		    header('HTTP/1.0 401 Unauthorized');
			$message = [
				'status' => FALSE,
				'msg' => 'Login gagal, username atau password salah.'
			];
			$this->print_json($message, utility::HTTP_UNAUTHORIZED);
		    exit;
		} else 
		{
		    $username = $_SERVER['PHP_AUTH_USER'];
		    $password =  $_SERVER['PHP_AUTH_PW'];

		    $result = $this->checkUser($username, $password);
			
			if (!empty($result)) 
			{
				return $result;
			}else
			{
				$message = [
					'status' => FALSE,
					'msg' => 'Login gagal, username atau password salah.'
				];
				$this->print_json($message, utility::HTTP_UNAUTHORIZED);
				exit;
			}
		}
	}

	public function tes()
	{
		echo "string";
	}

	

}

/* End of file app_helper.php */
/* Location: ./application/controllers/admin/app_helper.php */