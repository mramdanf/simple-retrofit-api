<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_credentials extends CI_Model {

	private $cred_table = 'credentials';

	function get_creds()
	{
		return $this->db->get($this->cred_table)->result();
	}

	function create_cred($data)
	{
		return $this->db->insert($this->cred_table, $data);
	}

	function update_cred($data)
	{
		return $this->db->update($this->cred_table, $data, array('id'=>$data['id']));
	}

	function delete_cred($id)
	{
		return $this->db->delete($this->cred_table, array('id'=>$id));
	}

}

/* End of file m_credentials.php */
/* Location: ./application/models/m_credentials.php */