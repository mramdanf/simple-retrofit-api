<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_account extends CI_Model {

	private $user_table = 'user';


	function create_user($data)
	{
		$username = $data['username'];
		$password = md5($data['password']);

		$is_user_exist = $this->db->get_where($this->user_table, array('username'=>$username, 'password'=>$password));
		if ($is_user_exist->num_rows() > 0) {
			return 'user_exist';
		}else {
			$data['password'] = md5($data['password']);
			$is_created = $this->db->insert($this->user_table, $data);
			if ($is_created) return 'user_modified';
			else return 'some_thing_error';
		}
		
	}

	function update_user($data)
	{
		$data['password'] = md5($data['password']);
		$is_updated = $this->db->update($this->user_table, $data, array('id'=>$data['id']));
		if ($is_updated) return 'user_modified';
		else return 'some_thing_error';
	}

	function get_users()
	{
		return $this->db->get($this->user_table)->result();

	}

	function delete_user($id)
	{
		return $this->db->delete($this->user_table, array('id'=>$id));
	}

	function auth($data)
	{
		$result = $this->db->get_where($this->user_table, 
			array('username'=>$data['username'], 'password'=>md5($data['password'])))->result();

		if ($result) 
		{
			return $result;

		}else { 
			return FALSE;
		}
	}

}

/* End of file m_account.php */
/* Location: ./application/models/m_account.php */