<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_account');
	}

	function post()
	{
		$id = $this->input->post('id');
		$data = $this->input->post();

		if ($id == NULL) {
			$res = $this->m_account->create_user($data);
		}else
		{
			$res = $this->m_account->update_user($data);
		}

		if ($res != 'user_exist') {
			if ($res != 'some_thing_error')
			{
				$response = array(
					utility::STATUS => TRUE,
					utility::RET_DATA => 'Berhasil modify data'
					);
				$this->utility->print_json($response, utility::HTTP_OK);
			}else
			{
				$response = array(
					utility::STATUS => FALSE,
					utility::RET_DATA => 'Gagal modify data'
					);
				$this->utility->print_json($response, utility::HTTP_INTERNAL_SERVER_ERROR);
			}
		}else 
		{
			$response = array(
				utility::STATUS => TRUE,
				utility::RET_DATA => 'Username and password exist, try another.'
				);
			$this->utility->print_json($response, utility::HTTP_BAD_REQUEST);
		}
	}

	function get()
	{
		$res = $this->m_account->get_users();
		if ($res) 
		{
			$response = array(
				utility::STATUS => TRUE,
				utility::RET_DATA => $res
				);
			$this->utility->print_json($response, utility::HTTP_OK);
		}else
		{
			$response = array(
				utility::STATUS => FALSE,
				utility::RET_DATA => 'no data'
				);
			$this->utility->print_json($response, utility::HTTP_NOT_FOUND);
		}
	}

	function delete()
	{
		$id = $this->input->post('id');
		if ($this->m_account->delete_user($id)) 
		{
			$response = array(
				'status' => TRUE,
				'msg' => 'Data berhasil dihapus.'
				);
			$this->utility->print_json($response, utility::HTTP_OK);
		}else
		{
			$response = array(
				'status' => FALSE,
				'msg' => 'internal server error'
				);
			$this->utility->print_json($response, utility::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	// for login
	// http://localhost/simple_retrofit/account/auth
	function auth()
	{
		// username, password
		$data = $this->input->post();

		$result = $this->m_account->auth($data);

		if ($result) 
		{
			$response = array(
				utility::STATUS => TRUE,
				utility::USER_DETAIL => $result
				);
			$this->utility->print_json($response, utility::HTTP_OK);
		}else
		{
			$response = array(
				utility::STATUS => FALSE,
				utility::USER_DETAIL => array()
				);
			$this->utility->print_json($response, utility::HTTP_UNAUTHORIZED);
		}
	}


// =========== login response ============

// {
//   "status": true,
//   "user_detail": [
//     {
//       "id": "1",
//       "first_name": "ramdan",
//       "last_name": "firdaus",
//       "username": "ramdan",
//       "password": "47bce5c74f589f4867dbd57e9ca9f808"
//     }
//   ]
// }

// {
//   "status": false,
//   "user_detail": [0]
// }

}

/* End of file account.php */
/* Location: ./application/controllers/account.php */