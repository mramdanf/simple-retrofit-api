<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Credentials extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_credentials');
		$this->utility->credential();
	}

	// http://localhost/simple_retrofit/credentials/get
	function get()
	{
		$result = $this->m_credentials->get_creds();

		if ($result) {
			$response = array(
				utility::STATUS => TRUE,
				utility::CRED_DATA => $result
				);
			$this->utility->print_json($response, utility::HTTP_OK);
		} else 
		{
			$response = array(
				utility::STATUS => FALSE,
				utility::CRED_DATA => 'no data'
				);
			$this->utility->print_json($response, utility::HTTP_NOT_FOUND);
		}
	}

	// http://localhost/simple_retrofit/credentials/post
	function post()
	{
		$id = $this->input->post('id');
		$data = $this->input->post();

		if ($id == NULL || $id == 0) 
		{
			$result = $this->m_credentials->create_cred($data);
		}else
		{
			$result = $this->m_credentials->update_cred($data);
		}

		if ($result)
		{
			$response = array(
				utility::STATUS => TRUE,
				utility::POST_MSG => 'Berhasil modify data'
				);
			$this->utility->print_json($response, utility::HTTP_OK);
		}else
		{
			$response = array(
				utility::STATUS => FALSE,
				utility::POST_MSG => 'Gagal modify data'
				);
			$this->utility->print_json($response, utility::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	// http://localhost/simple_retrofit/credentials/delete
	function delete()
	{
		$id = $this->input->post('id');

		if ($id != NULL && $id != 0) {
			$result = $this->m_credentials->delete_cred($id);
		}else
		{
			$response = array(
				utility::STATUS => FALSE,
				utility::POST_MSG => 'No id found.'
				);
			$this->utility->print_json($response, utility::HTTP_BAD_REQUEST);
		}

		if($result)
		{
			$response = array(
				utility::STATUS => TRUE,
				utility::POST_MSG => 'Berhasil delete data'
				);
			$this->utility->print_json($response, utility::HTTP_OK);
		}else
		{
			$response = array(
				utility::STATUS => FALSE,
				utility::POST_MSG => 'Gagal delete data'
				);
			$this->utility->print_json($response, utility::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

// ========== get data ===========
// {
//   "status": true,
//   "credential_data": [
//     {
//       "id": "2",
//       "description": "akun bitbucket",
//       "username": "ramdan",
//       "password": "aaa"
//     }
//   ]
// }

// ========== post and data ==========
// {
//   "status": true,
//   "post_msg": "Berhasil modify data"
// }

}

/* End of file credentials.php */
/* Location: ./application/controllers/credentials.php */